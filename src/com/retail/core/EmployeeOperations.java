package com.retail.core;

import java.util.ArrayList;
import java.util.List;

public class EmployeeOperations {

	public static List<Employee> getAllEmployees() {
		List<Employee> empList = new ArrayList<>();

		Employee emp1 = new Employee();
		emp1.setFullName("Ned S");
		emp1.setId(1);
		emp1.setHoursWorked(40);
		if (emp1.getHoursWorked() <= 40) {
			empList.add(emp1);
		}

		Employee emp2 = new Employee();
		emp2.setFullName("Rob c");
		emp2.setId(2);
		emp2.setHoursWorked(20);
		if (emp2.getHoursWorked() <= 40) {
			empList.add(emp2);
		}

		Employee emp3 = new Employee();
		emp3.setFullName("Kit h");
		emp3.setId(3);
		emp3.setHoursWorked(30);
		if (emp3.getHoursWorked() <= 40) {
			empList.add(emp3);
		}

		Employee emp4 = new Employee();
		emp4.setFullName("Arya s");
		emp4.setId(4);
		emp4.setHoursWorked(40);
		if (emp4.getHoursWorked() <= 40) {
			empList.add(emp4);
		}

		Employee emp5 = new Employee();
		emp5.setFullName("Jon s");
		emp5.setId(5);
		emp5.setHoursWorked(50);
		if (emp5.getHoursWorked() <= 40) {
			empList.add(emp5);
		}

		return empList;

	}

	public static int getEmployeeIDByName(String name) {
		int id = 0;
		List<Employee> empList = getAllEmployees();
		for (Employee employees : empList) {
			if (employees.getFullName().equals(name)) {
				id = employees.getId();
			}
		}
		return id;
	}

}
