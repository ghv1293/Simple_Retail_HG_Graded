Feature: Simple Retail Web


	
Scenario: Check Price
	Given shipping items
	When I give upc and price
|"567321101987"	|"19.99"	|	
|"567321101986"	|"17.99"	|
|"567321101985"	|"20.49"	|
|"567321101984"	|"23.88"	|
|"467321101899"	|"9.75"		|
|"477321101878"	|"17.25"	|
	Then check weather they match on the Shipment Page
	
Scenario: Check Worked hours
	Given employee list
	Then check if they work hours more than 40
	
Scenario: Check All Employess Appear Drop down
	Given Shipment page
	When I give the list
	|Ned S|
	|Rob c|
	|Kit h|
	|Arya s| 
	Then check if they match the drop down
	
Scenario: Check Jenkins
	Given Jenkins url
	
	Then take screenshot Jenkins
	
Scenario: Check git
	Given Git url
	
	Then take screenshot Git