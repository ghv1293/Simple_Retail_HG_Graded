<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Shipment JSP</title>
</head>
<body>

	<h3>Check the items that you want to include in this shipment.</h3>
	<br>

	<form name='createShipment' action="ShippingServlet" method="Post">

		<table border='1' id ="shipmentsTable">
			<c:forEach var="item" items="${sessionScope.items}">
				<tr>
					<td><input id="checkedRows" name="checkedRows" type="checkbox"
						value="${item.upc}"></td>
					<td>${item.upc}</td>
					<td>${item.description}</td>
					<td>${item.price}</td>
					<td>${item.weight}</td>
					<td>${item.shippingType}</td>
				</tr>
			</c:forEach>
		</table>
		<br>
		<select name="selectedEmp" id="empDropDown">
		<c:forEach var="employee" items="${sessionScope.employees}">
		<option value="${employee.fullName}">${employee.fullName}</option>
		</c:forEach>
		</select>
		<br><br>
		<br> <input type='submit' value="Create Shipment">
	</form>
</body>
</html>