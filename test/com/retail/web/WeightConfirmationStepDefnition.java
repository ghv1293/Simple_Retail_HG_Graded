package com.retail.web;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import com.retail.core.Employee;
import com.retail.core.EmployeeOperations;
import com.retail.core.Item;
import com.retail.core.Products;
import com.retail.core.Shipment;

import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class WeightConfirmationStepDefnition {

	private Map<String, String> upcList;
	private Map<String, String> itemsList;
	private List<String> empList;
	EmployeeOperations empOperation;
	WebDriver driver;

	@Before("@Web")
	public void setUp() {
		String os = System.getProperty("os.name").toLowerCase().split("")[0];

		if ("mac".contains(os)) {
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/chromedriver");
		} else {
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\chromedriver.exe"); // make
																													// sure
																													// to
																													// define
																													// where
																													// your
																													// chromedriver.exe
																													// is!!!!
		}

	}

	@Given("^shipping items$")
	public void shipping_items() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		driver = new ChromeDriver();
		driver.get("http://localhost:8080/SimpleRetailWeb/ShippingServlet?");
		WebElement table_element = driver.findElement(By.id("shipmentsTable"));
		List<WebElement> tr_collection = table_element.findElements(By.xpath("id('shipmentsTable')/tbody/tr"));
		itemsList = new HashMap<>();
		System.out.println("NUMBER OF ROWS IN THIS TABLE = " + tr_collection.size());
		int row_num, col_num;
		row_num = 1;
		String upcKey = null;
		for (WebElement trElement : tr_collection) {
			List<WebElement> td_collection = trElement.findElements(By.xpath("td"));
			// System.out.println("NUMBER OF COLUMNS="+td_collection.size());
			col_num = 1;
			for (WebElement tdElement : td_collection) {
				System.out.println("row # " + row_num + ", col # " + col_num + "text=" + tdElement.getText());
				if (col_num == 2) {
					upcKey = '"' + tdElement.getText() + '"';
				}
				if (col_num == 4) {
					itemsList.put(upcKey, '"' + tdElement.getText() + '"');
				}
				col_num++;
			}
			row_num++;
		}
		System.out.println("Table data");
		for (Map.Entry<String, String> entry : itemsList.entrySet()) {
			System.out.println(entry.getKey() + " : " + entry.getValue());
		}
	}

	@When("^I give upc and price$")
	public void i_give_upc_and_price(Map<String, String> upcList) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// For automatic transformation, change DataTable to one of
		// List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
		// E,K,V must be a scalar (String, Integer, Date, enum etc)
		this.upcList = upcList;
		System.out.println("Secnario Data");
		for (Map.Entry<String, String> entry : upcList.entrySet()) {
			System.out.println(entry.getKey() + " : " + entry.getValue());
		}
	}

	@Then("^check weather they match on the Shipment Page$")
	public void check_weather_they_match_on_the_Shipment_Page() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		Assert.assertEquals(true, upcList.equals(itemsList));
		System.out.println(upcList.equals(itemsList));
		Thread.sleep(1000);
		driver.close();
		driver.quit();
	}

	@Given("^employee list$")
	public void employee_list() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		empOperation = new EmployeeOperations();
	}

	@Then("^check if they work hours more than (\\d+)$")
	public void check_if_they_work_hours_more_than(int arg1) throws Throwable {
		List<Employee> empList = empOperation.getAllEmployees();
		boolean expected = true;
		boolean actual = true;
		for (Employee em : empList) {
			if (em.getHoursWorked() > 40) {
				actual = false;
			}
		}
		Assert.assertEquals(expected, actual);
	}

	@Given("^Shipment page$")
	public void shipment_page() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		driver = new ChromeDriver();
		driver.get("http://localhost:8080/SimpleRetailWeb/ShippingServlet?");
	}

	@When("^I give the list$")
	public void i_give_the_list(List<String> empList) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// For automatic transformation, change DataTable to one of
		// List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
		// E,K,V must be a scalar (String, Integer, Date, enum etc)
		this.empList = empList;
	}

	@Then("^check if they match the drop down$")
	public void check_if_they_match_the_drop_down() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		Select empDropDown = new Select(driver.findElement(By.id("empDropDown")));
		Assert.assertEquals(true, empList.size() == empDropDown.getOptions().size());
		driver.close();
		driver.quit();
	}

	@Given("^Jenkins url$")
	public void jenkins_url() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		driver = new ChromeDriver();
		driver.get("http:localhost:8081");
	}

	@Then("^take screenshot Jenkins$")
	public void take_screenshot_Jenkins() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		driver.findElement(By.id("j_username")).sendKeys("ghv1293");
		driver.findElement(By.name("j_password")).sendKeys("ABCabc012");
		driver.findElement(By.id("yui-gen1-button")).click();
		File scrFile=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(scrFile, new File("C:\\Users\\hgundubo\\Desktop\\jenkins.jpg"));
			driver.findElement(By.cssSelector("a[href*='logout']")).click();
		}finally {
			driver.close();
			driver.quit();
		}


	}
	@Given("^Git url$")
	public void git_url() throws Throwable {
			driver = new ChromeDriver();

		driver.get("https://gitlab.com/ghv1293/Simple_Retail_HG_Graded");
	}
	@Then("^take screenshot Git$")
	public void take_screenshot_Git() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		File scrFile=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(scrFile, new File("C:\\Users\\hgundubo\\Desktop\\git.jpg"));
		}finally {
			driver.close();
			driver.quit();
		}
	}

}
