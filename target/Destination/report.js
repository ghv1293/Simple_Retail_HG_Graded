$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("com/retail/web/weightConfirmation.feature");
formatter.feature({
  "line": 1,
  "name": "Simple Retail Web",
  "description": "",
  "id": "simple-retail-web",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 5,
  "name": "Check Price",
  "description": "",
  "id": "simple-retail-web;check-price",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 6,
  "name": "shipping items",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "I give upc and price",
  "rows": [
    {
      "cells": [
        "\"567321101987\"",
        "\"19.99\""
      ],
      "line": 8
    },
    {
      "cells": [
        "\"567321101986\"",
        "\"17.99\""
      ],
      "line": 9
    },
    {
      "cells": [
        "\"567321101985\"",
        "\"20.49\""
      ],
      "line": 10
    },
    {
      "cells": [
        "\"567321101984\"",
        "\"23.88\""
      ],
      "line": 11
    },
    {
      "cells": [
        "\"467321101899\"",
        "\"9.75\""
      ],
      "line": 12
    },
    {
      "cells": [
        "\"477321101878\"",
        "\"17.25\""
      ],
      "line": 13
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "check weather they match on the Shipment Page",
  "keyword": "Then "
});
formatter.match({
  "location": "WeightConfirmationStepDefnition.shipping_items()"
});
formatter.result({
  "duration": 5828604882,
  "status": "passed"
});
formatter.match({
  "location": "WeightConfirmationStepDefnition.i_give_upc_and_price(String,String\u003e)"
});
formatter.result({
  "duration": 2987424,
  "status": "passed"
});
formatter.match({
  "location": "WeightConfirmationStepDefnition.check_weather_they_match_on_the_Shipment_Page()"
});
formatter.result({
  "duration": 1676331239,
  "status": "passed"
});
formatter.scenario({
  "line": 16,
  "name": "Check Worked hours",
  "description": "",
  "id": "simple-retail-web;check-worked-hours",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 17,
  "name": "employee list",
  "keyword": "Given "
});
formatter.step({
  "line": 18,
  "name": "check if they work hours more than 40",
  "keyword": "Then "
});
formatter.match({
  "location": "WeightConfirmationStepDefnition.employee_list()"
});
formatter.result({
  "duration": 11385965,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "40",
      "offset": 35
    }
  ],
  "location": "WeightConfirmationStepDefnition.check_if_they_work_hours_more_than(int)"
});
formatter.result({
  "duration": 8055698,
  "status": "passed"
});
formatter.scenario({
  "line": 20,
  "name": "Check All Employess Appear Drop down",
  "description": "",
  "id": "simple-retail-web;check-all-employess-appear-drop-down",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 21,
  "name": "Shipment page",
  "keyword": "Given "
});
formatter.step({
  "line": 22,
  "name": "I give the list",
  "rows": [
    {
      "cells": [
        "Ned S"
      ],
      "line": 23
    },
    {
      "cells": [
        "Rob c"
      ],
      "line": 24
    },
    {
      "cells": [
        "Kit h"
      ],
      "line": 25
    },
    {
      "cells": [
        "Arya s"
      ],
      "line": 26
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 27,
  "name": "check if they match the drop down",
  "keyword": "Then "
});
formatter.match({
  "location": "WeightConfirmationStepDefnition.shipment_page()"
});
formatter.result({
  "duration": 3995736729,
  "status": "passed"
});
formatter.match({
  "location": "WeightConfirmationStepDefnition.i_give_the_list(String\u003e)"
});
formatter.result({
  "duration": 102325,
  "status": "passed"
});
formatter.match({
  "location": "WeightConfirmationStepDefnition.check_if_they_match_the_drop_down()"
});
formatter.result({
  "duration": 771248012,
  "status": "passed"
});
formatter.scenario({
  "line": 29,
  "name": "Check Jenkins",
  "description": "",
  "id": "simple-retail-web;check-jenkins",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 30,
  "name": "Jenkins url",
  "keyword": "Given "
});
formatter.step({
  "line": 32,
  "name": "take screenshot Jenkins",
  "keyword": "Then "
});
formatter.match({
  "location": "WeightConfirmationStepDefnition.jenkins_url()"
});
formatter.result({
  "duration": 4277636688,
  "status": "passed"
});
formatter.match({
  "location": "WeightConfirmationStepDefnition.take_screenshot_Jenkins()"
});
formatter.result({
  "duration": 2567686741,
  "status": "passed"
});
formatter.scenario({
  "line": 34,
  "name": "Check git",
  "description": "",
  "id": "simple-retail-web;check-git",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 35,
  "name": "Git url",
  "keyword": "Given "
});
formatter.step({
  "line": 37,
  "name": "take screenshot Git",
  "keyword": "Then "
});
formatter.match({
  "location": "WeightConfirmationStepDefnition.git_url()"
});
formatter.result({
  "duration": 5260127864,
  "status": "passed"
});
formatter.match({
  "location": "WeightConfirmationStepDefnition.take_screenshot_Git()"
});
formatter.result({
  "duration": 1725275597,
  "status": "passed"
});
});